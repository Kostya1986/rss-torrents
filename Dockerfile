FROM node:16-alpine

RUN apk update && \
    apk --no-cache upgrade && \
    apk add --no-cache redis

WORKDIR /usr/src/app

RUN npm install -g pm2

COPY package.json package-lock.json ./
RUN npm ci --production

LABEL maintainer="Konstantin Shuplenkov"
LABEL description="RSS torrents watcher and downloader"

ARG NODE_ENV=production
ENV NODE_ENV ${NODE_ENV}

COPY . .

EXPOSE 8080

CMD ["sh", "src/script/docker.sh"]

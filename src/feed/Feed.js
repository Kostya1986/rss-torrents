const Parser = require('rss-parser');
const stringify = require('json-stringify-deterministic');
const redis = require('../utils/redis');

class Feed {
  constructor(url) {
    this.url = url;
    this.feed = null;
    this.parser = new Parser();
    this.loaded = false;
    this.key = `feeds:${this.url}`;
    this.oldFeed = null;
  }

  async load() {
    this.feed = await this.parser.parseURL(this.url);
    this.oldFeed = JSON.parse(await redis.get(this.key));
    this.loaded = true;

    return this;
  }

  checkIsLoaded() {
    if (!this.loaded) {
      throw new Error('RSS is not loaded');
    }
  }

  isUpdated() {
    this.checkIsLoaded();

    return !this.oldFeed || stringify(this.feed) !== stringify(this.oldFeed);
  }

  async save() {
    this.checkIsLoaded();

    await redis.set(this.key, stringify(this.feed), 'EX', 60 * 60 * 24 * 30); // 1 month

    return this;
  }

  getUpdatedItems() {
    this.checkIsLoaded();

    if (!this.oldFeed) {
      return this.feed.items;
    }

    return this.feed.items.filter((item) => {
      const stringifiedItem = stringify(item);

      return !this.oldFeed.items.find((oldItem) => stringify(oldItem) === stringifiedItem);
    });
  }
}

module.exports = Feed;

#!/bin/bash

mkdir -p /data/rss-torrents
/usr/bin/redis-server --bind '127.0.0.1' --daemonize yes --dir /data/rss-torrents

pm2 --no-daemon start pm2.json --env production

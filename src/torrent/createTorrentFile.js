const { writeFile } = require('fs').promises;
const path = require('path');
const Magnet2torrent = require('magnet2torrent-js');
const logger = require('../utils/logger');

/**
 *
 * @param {string} magnet
 * @param {string} pathToSave
 * @return {Promise<void>}
 */
async function createTorrentFile(magnet, pathToSave) {
  if (!magnet.startsWith('magnet:')) {
    logger.error(`The link is not a magnet link: ${magnet}`);

    return;
  }

  try {
    const m2t = new Magnet2torrent({ timeout: 120 });
    const torrent = await m2t.getTorrent(magnet);
    const fileToSave = path.join(pathToSave, `${torrent.name}.torrent`);
    await writeFile(fileToSave, torrent.toTorrentFile());

    logger.info(`New torrent saved to ${fileToSave}`);
  } catch (e) {
    logger.error(`Error creating torrent file: ${e.message}`);
  }
}

module.exports = createTorrentFile;

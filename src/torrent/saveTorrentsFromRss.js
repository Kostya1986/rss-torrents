const Feed = require('../feed/Feed');
const createTorrentFile = require('./createTorrentFile');
const logger = require('../utils/logger');

async function saveTorrentsFromRss(rssUrl, pathToSave = '') {
  const feed = new Feed(rssUrl);

  logger.info(`Loading ${rssUrl}`);

  try {
    await feed.load();
  } catch (e) {
    logger.error(`Error loading ${rssUrl}: ${e.message}`);

    return;
  }

  logger.info(`Loaded`);

  const updatedItems = feed.getUpdatedItems();

  logger.info(`Found ${updatedItems.length} new ${updatedItems.length > 1 ? 'records' : 'records'}`);

  if (updatedItems.length > 0) {
    await Promise.all(
      updatedItems.map(async (item) => {
        logger.info(`Processing ${item.title}`);

        return createTorrentFile(item.link, pathToSave);
      }),
    );

    await feed.save();

    logger.log('New torrents were saved');
  }
}

module.exports = saveTorrentsFromRss;

const path = require('path');
const Queue = require('bull');
const redis = require('../utils/redis');
const logger = require('../utils/logger');
const { MINUTES } = require('../constants/keys');

const rssProcessingQueue = new Queue('rss processing');

rssProcessingQueue.process(path.join(__dirname, 'processTorrents.js'));

(async () => {
  let minutes = await redis.get(MINUTES);
  if (minutes === null || Number.isNaN(parseInt(minutes, 10))) {
    minutes = 30;
  }

  rssProcessingQueue.add({}, { repeat: { cron: `0 */${parseInt(minutes, 10)} * * * *` } });

  logger.log('Watcher was started');
})();

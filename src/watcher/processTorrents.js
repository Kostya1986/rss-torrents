const saveTorrentsFromRss = require('../torrent/saveTorrentsFromRss');
const redis = require('../utils/redis');
const logger = require('../utils/logger');
const keys = require('../constants/keys');

async function processTorrents() {
  logger.info(`${new Date()} Processing...`);

  const urlsString = await redis.get(keys.FEEDS);
  const pathToSave = await redis.get(keys.PATH);

  if (urlsString === null) {
    logger.error('Please define at least one feed url');

    return;
  }

  if (pathToSave === null) {
    logger.error('Please define path to save torrents');

    return;
  }

  const urls = await JSON.parse(urlsString);

  logger.info(`Found ${urls.length} ${urls.length > 1 ? 'urls' : 'url'} to parse`);

  if (urls.length < 1) {
    return;
  }

  for (const url of urls) {
    await saveTorrentsFromRss(url, pathToSave);
  }
}

module.exports = processTorrents;

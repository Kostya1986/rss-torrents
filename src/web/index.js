const path = require('path');
const Koa = require('koa');
const router = require('@koa/router')();
const koaBody = require('koa-body');
const serve = require('koa-static');
const isAbsoluteUrl = require('is-absolute-url');

const render = require('./lib/render');
const redis = require('../utils/redis');
const keys = require('../constants/keys');
const logger = require('../utils/logger');
const processTorrents = require('../watcher/processTorrents');

const PORT = 8080;

const app = new Koa();

app.use(render);

app.use(koaBody());

app.use(serve(path.join(__dirname, 'style')));

async function show(ctx) {
  const savePath = await redis.get(keys.PATH);
  const feedsString = await redis.get(keys.FEEDS);

  const feeds = feedsString === null ? [] : JSON.parse(feedsString);

  await ctx.render('index', { path: savePath, feeds: feeds.join('\n') });
}

async function create(ctx) {
  let saved = false;
  const { path: savePath, feeds: rawFeeds } = ctx.request.body;

  const feeds = rawFeeds
    .split('\n')
    .map((feed) => feed.trim())
    .filter((feed) => feed !== '' && isAbsoluteUrl(feed));

  if (path !== '') {
    await redis.set(keys.PATH, savePath.trim());

    saved = true;
  }

  if (feeds.length) {
    await redis.set(keys.FEEDS, JSON.stringify(feeds));

    saved = true;
  }

  if (saved) {
    await redis.save();

    // don't need await here
    processTorrents();
  }

  ctx.redirect('/');
}

router.get('/', show).post('/', create);

app.use(router.routes());

app.listen(PORT);

logger.log(`Web server was started at ${8080} port`);
